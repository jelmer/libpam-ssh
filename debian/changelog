libpam-ssh (2.3+ds-6) unstable; urgency=medium

  * RC bug fix release (Closes: #1003617), correct d/pam-configs/ssh-*.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 13 Jan 2022 19:58:56 +0000

libpam-ssh (2.3+ds-5) unstable; urgency=medium

  * Debianization:
    - d/patches/*:
      - d/p/upstream-fix-session_closing.patch, introduce
        (Closes: #802212);
      - d/p/upstream-harden-use_opendir.patch, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 09 Jan 2022 20:27:23 +0000

libpam-ssh (2.3+ds-4) unstable; urgency=medium

  * Debianization:
    - d/pam-configs/*:
      - d/pc/ssh-{pwd,client,server}, introduce (Closes: # 784658);
      - d/pc/ssh, obsoleted
    - d/patches/*:
      - d/p/upstream-fix-ssh_session-file_flag.patch, introduce
        (Closes: #828739);
    - d/{NEWS,README.Debian}, update;
    - d/prerm, refresh;
    - d/libpam-ssh.install, adapt;
    - d/copyright:
      - Copyright year tuples, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 09 Jan 2022 16:08:33 +0000

libpam-ssh (2.3+ds-3) unstable; urgency=medium

  * Debianization:
    - d/control:
      - Standard version, bump to 4.6.0 (no change);
    - d/copyright:
      - duplicate-globbing-patterns linitian emits, fix;
    - d/libpam-ssh.manpages, correct;
    - d/NEWS.Debian, renamed d/NEWS;
    - d/docs, renamed libpam-ssh.docs;
    - d/libpam-ssh.{lintian-overrides,manpages}, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 19 Dec 2021 12:59:17 +0000

libpam-ssh (2.3+ds-2) unstable; urgency=medium

  * FTBFS fix version, gcc10 migration (Closes: #957464).
  * Debianization:
    - d/control:
      - debhelper, bump to 12 (no change);
      - Standard version, bump to 4.5.0 (no change);
    - d/copyright:
      - Copyright year tuple, update;
    - d/patches/*:
      - d/p/upstream-fix-gcc10.patch, introduce;
      - d/p/debianization*.patch:
        - Forwarded field, specify as 'not-needed';
    - d/u/metatdata, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 12 Nov 2020 21:07:24 +0000

libpam-ssh (2.3+ds-1) unstable; urgency=medium

  * New upstream version.
  * Debianization:
    - debian/patches/*:
      - d/p/upstream-fix-drop_support-extra.patch, integrated;
      - d/p/upstream-manpage-spelling-error-correction.patch, partially
        integrated;
      - d/p/debianization.patch, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 25 Jan 2019 05:16:19 +0000

libpam-ssh (2.2+ds-2) unstable; urgency=medium

  * Debianization:
    - debian/patches/*:
      - d/p/upstream-fix-drop_support-extra.patch, revisit and submit.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 14 Jan 2019 10:28:06 +0000

libpam-ssh (2.2+ds-1) unstable; urgency=medium

  * New upstream version.
  * Debianization:
    - debian/NEWS.Debian, advert dop support for SSH1 and RSA1;
    - debian/copyright:
      - Copyright year tuple, update;
      - Files-Excluded, refresh;
      - Sourde field, secure URL;
      - Files fields, refresh;
    - debian/watch:
      - repacksuffix options, reset index to zero (none);
    - debian/patches/*:
      - d/p/upstream-migratio-opensll11.patch, integrated;
      - d/p/upstream-fix-drop_support-extra.patch, introduce and submit;
      - d/p/debianization.patch, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 12 Jan 2019 11:54:48 +0000

libpam-ssh (2.1+ds1-4) unstable; urgency=medium

  [ Jerome Benoit ]
  * d/p/upstream-migratio-opensll11.patch: Introduce (Closes: #859054)
  * d/control: Build-Depends field: Refresh
  * d/control: Standards Version: Bump to 4.3.0 (no change)
  * d/u/signing-key.asc: Render minimal
  * d/s/options: Remove

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/watch: Use https protocol

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 25 Dec 2018 15:58:52 +0000

libpam-ssh (2.1+ds1-3) unstable; urgency=medium

  * Debianization:
    - debian/copyright:
      - Format field, secure;
      - Copyright fields, update year tuples;
    - debian/control:
      - debhelper, bump to 11 (refresh d/control and d/rules);
      - Standards Version, bump to 4.1.4 (no change);
      - Vcs-* fields, migration to Salsa;
    - debian/watch:
      - version, bump to 4;
      - compression option, add;
    - debian/rules:
      - debhelper targets, refresh;
      - get-orig-source target, discard;
    - debian/upstream/, migrate to clear key;
    - debian/source/include-binaries, discard;
    - debian/patches/*:
      - d/p/debianization-manpages.patch , correct typo in header.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 03 Jun 2018 03:42:47 +0000

libpam-ssh (2.1+ds1-2) unstable; urgency=medium

  * RC bug fix release (Closes: #828405): follow the Debian OpenSSH Maintainers
    approach as exposed in Debian Bug report #828475.
  * Debianization:
    - debian/control;
      - Standards-Version, bump to 3.9.8;
      - Build-Depends, adjust to force use of OpenSSL 1.0 for now
        (see RC bug fix comment);
      - Vcs-* fields, secure;
      - #Vcs-Hg field, discard;
    - debian/copyright, refresh;
    - debian/rules:
      - dpkg-buildflags, add hardening=+all.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 13 Nov 2016 20:51:17 +0000

libpam-ssh (2.1+ds1-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release:
    - all non-centric Debian patches were incorporated, thanks to the
      upstream maintainer Wolfgang Rosenauer <wr@rosenauer.org>.
  * Debianization:
    - debian/control, Vcs-Browser field correction;
    - debian/copyright:
      - repack by using the Files-Excluded machinery instead
        of the ad-hoc script debian/repack;
      - refresh;
    - debian/repack, discard (see above);
    - debian/rules, refresh;
    - debian/watch:
      - revisit (see above);
      - adapt to intuitive but non-machine upstream versioning scheme;
    - debian/source/lintian-override: update (read: discard).

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 11 Dec 2015 23:09:09 +0000

libpam-ssh (2.01+ds-2) experimental; urgency=medium

  * The `explicit key sort' policy now respects the former `no SSH login
    keys' policy.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 13 Apr 2015 09:44:09 +0000

libpam-ssh (2.01+ds-1) experimental; urgency=medium

  * Debianization:
    - debian/copyright: refresh;
    - debian/repack: add to permit cleaner git-buildpackage processes;
    - debian/watch: add repack related features;
    - debian/rules: clean up;
    - debian/patches/debianization.patch: further strip the pam module
        by removing unused objects;
    - debian/patches/debianization-manpages.patch: refresh;
    - debian/NEWS.Debian, revisit to reflect changes;
    - gpg-signature check support: move signing key to debian/upstream/;
    - lintian check: neutralize and comment.
  * Update OpenSSH embedded code against OpenSSh-6.7p1 code.
  * Add ED25519 keys support.
  * Explicit key sort policy.
  * Fixes, updates and improvements submitted to the upstream maintainer.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 08 Mar 2015 16:04:48 +0000

libpam-ssh (2.01-2) unstable; urgency=medium

  * Debianization:
    - debian/copyright:
      - lintian complains, fix;
    - debian/control:
      - Standards Version, bump to 3.9.6 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 23 Oct 2014 14:01:43 +0000

libpam-ssh (2.01-1) unstable; urgency=low

  * New upstream release:
    - all non-centric Debian patches were incorporated, thanks to the
      upstream maintainer Wolfgang Rosenauer <wr@rosenauer.org>.
  * Debianization:
    - debian/control:
      - bump Standards Version to 3.9.5;
      - Vcs-Browser field correction;
    - debian/patches/*:
      - adapt manpages;
      - refresh;
    - add gpg-signature check support;
    - pam_ssh.8 manpage is now patched.
  * Fix conflicting declarations as reported by goto-cc (Closes: #749726).
  * Minor fixes submitted to the upstream maintainer.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 31 May 2014 08:37:37 +0000

libpam-ssh (2.0-1) unstable; urgency=low

  * New upstrean release:
    - all non-centric Debian patches were incorporated, thanks to the
      upstream maintainer Wolfgang Rosenauer <wr@rosenauer.org>;
    - introduce $HOME/.ssh/session-keys.d as session counterpart of
      $HOME/.ssh/login-keys.d per-user authentication key folder
      inherited from Debian patches.
  * Debianization:
    - debian/watch, update URL path regex to consider customary
        compression formats (gz,bz2,xz);
    - debian/rules, upgrade to specify the ssh-agent SGID group name
        effectively used in Debian (ssh);
    - debian/{pam_ssh.8,README.Debian}:
      - update to introduce the per-user session key folder;
      - refresh and clarify (Closes: #718435).
  * Minor fixes submitted to the upstream maintainer.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 21 Nov 2013 05:26:23 +0000

libpam-ssh (1.98-2) unstable; urgency=low

  * Set ssh-agent real GID to ssh group GID, as ssh-agent is installed
    setgid wrt to the ssh group; it allows one to pass (and to honour)
    environment variables otherwise discarded by glibc, as TMPDIR.
  * Fix ssh-agent TMPDIR honouring, see previous enhancement; TMP is
    no more honoured to stick closer to openssh approach.
  * Handle inexistent per-user configuration direction for session phase
    by shortcutting the process.
  * Let the ssh-agent to determine the appropriate shell style; this is
    now possible because the ssh-agent now possesses the user UID.
  * Move silent-ssh-single-sign-on into debian/pam-configs and rename it
    ssh wrt to the emerging custom.
  * Revisit README.Debian and NEWS.Debian to reflect changes.
  * Refresh pam_ssh manpage.
  * debian/control, Vcs-* headers canonicalization.
  * Slightly improve log messaging for closing sessions.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 14 Jul 2013 13:35:55 +0000

libpam-ssh (1.98-1) unstable; urgency=low

  * New upstream release, which obsoletes some of the previous patches.
  * Update ssh-agent Debian specific starter accordingly.
  * debian/control, Vcs-* headers added.
  * Upload to unstable.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 10 May 2013 14:03:48 +0000

libpam-ssh (1.97-1) experimental; urgency=low

  * New upstream version.
  * Bump debhelper build-dep to >= 9.
  * Bump Standards Version to 3.9.4.
  * Rewrite debian/rules:
    - use dh sequencer with autoreconf support
    - hardened
  * Convert debian/copyright to DEP-5 format.
  * Revisit debian/control.
  * Add optional get-orig-source target in debian/rules to get the currently
    packaged upstream tarball via uscan.
  * Add a default target in debian/rules which basically queries package status
    with uscan; output in DEHS format.
  * Update OpenSSH embedded code against OpenSSh-6.0p1 code.
  * Add ECDSA keys support.
  * Now files in $HOME/.ssh/login-keys.d with .disabled or .frozen as suffix
    are ignored.
  * Now TMPDIR and TMP are honoured when ssh-agent is spawned (implemented
    with libpam-tmpdir package in mind).
  * Revisit README.Debian and pam_ssh manpage to reflect changes.
  * Minor fixes and enhancements, thanks to Peter Marschall.
  * Conform debian/patches/ patches to DEP-3 format.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 23 Dec 2012 10:16:10 +0000

libpam-ssh (1.92-15) unstable; urgency=low

  * Reintroduction release. (Closes: #685042, #664177)
  * New maintainer.
  * Fix Release Critical bug caused by libtool. (Closes: #634577)
  * Fix Release Critical bug caused by missing dependency. (Closes: #603819)
  * Add multiarch support. (Release goal)

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 06 Nov 2012 02:18:40 +0000

libpam-ssh (1.92-14) unstable; urgency=low

  * Fix wrong use of pam-auth-update, thanks to Steve Langasek.
    (Closes: #582395)

 -- Jens Peter Secher <jps@debian.org>  Sun, 06 Jun 2010 00:10:09 +0200

libpam-ssh (1.92-13) unstable; urgency=low

  * Added versioned dependency on cron 3.0pl1-107 which use
    common-session-noninteractive in pam.d, thanks to Petter Reinholdtsen.
    (Closes: #574708)
  * Added versioned dependency on libpam-runtime to provide
    pam-auth-update, thank to Petter Reinholdtsen.
    (Closes: #579078)
  * Tighten up description.
    (Closes: #575601)

 -- Jens Peter Secher <jps@debian.org>  Sat, 01 May 2010 11:33:03 +0200

libpam-ssh (1.92-12) unstable; urgency=low

  * Fix MIPS build failure by applying the version-scipt at build time
    only.

 -- Jens Peter Secher <jps@debian.org>  Sat, 20 Mar 2010 11:27:29 +0100

libpam-ssh (1.92-11) unstable; urgency=low

  * Pass a debian/pam_ssh.version to LD so that only pam_* symbols are
    exported, thanks to Erich Schubert and Timo Sirainen.
    (Closes: 572266)

 -- Jens Peter Secher <jps@debian.org>  Sat, 13 Mar 2010 15:43:25 +0100

libpam-ssh (1.92-10) unstable; urgency=low

  * Use pam-auth-update together with the new setup file
    /usr/share/pam-configs/silent-ssh-single-sign-on to automatically
    enable the traditional functionality.  Please send additional profiles
    if you think that such profiles should be included in this package.
    (Closes: #383950, #566723)
  * Fixed manpage warning.
  * Bumped the Standards-Version to 3.8.4, no changes needed.

 -- Jens Peter Secher <jps@debian.org>  Sat, 27 Feb 2010 12:09:50 +0100

libpam-ssh (1.92-9) unstable; urgency=low

  * Converted to dpkg source format 3.0 (quilt).
  * Use mercurial-buildpackage for maintenance, hence a new location of
    the Mercurial directory.

 -- Jens Peter Secher <jps@debian.org>  Sun, 07 Feb 2010 15:34:03 +0100

libpam-ssh (1.92-8) unstable; urgency=low

  * Let the PAM session handle situations where there is no controlling
    tty, thanks to Ansgar Burchardt.
    (Closes: #541449)

 -- Jens Peter Secher <jps@debian.org>  Sat, 15 Aug 2009 16:09:29 +0200

libpam-ssh (1.92-7) unstable; urgency=low

  * Avoid leaking user names, see CVE-2009-1273.
    (Closes: 535877)
  * Do not refer to non-existing session config file in README, thanks
    to Andrei Popescu.
    (Closes: 537155)

 -- Jens Peter Secher <jps@debian.org>  Wed, 22 Jul 2009 13:59:21 +0200

libpam-ssh (1.92-6) unstable; urgency=low

  * Always unlock traditional SSH keys, but only use keys in
    login-keys.d to authenticate.
    (Closes: #519314)
  * Updated documentation to reflect the new behaviour.
  * Bumped the Standards-Version to 3.8.1.
  * Added a Vcs-Hg control filed to indicate the location of the public
    repository.

 -- Jens Peter Secher <jps@debian.org>  Sun, 29 Mar 2009 17:05:26 +0200

libpam-ssh (1.92-5) unstable; urgency=low

  * Upload to "squeeze".

 -- Jens Peter Secher <jps@debian.org>  Sat, 07 Mar 2009 10:16:49 +0100

libpam-ssh (1.92-4) experimental; urgency=low

  * Make detection of SSH login keys use standard file operation, thanks
    to Luca Niccoli and Steve McIntyre.
  * Do not ship any ssh-auth or ssh-session files in /etc/pam.d because it
    is just as easy and more flexible to add the lines directly in the
    relevant PAM configuration files, thanks to Sylvain Collilieux and
    Steve Langasek.
    (Closes: #325351)

 -- Jens Peter Secher <jps@debian.org>  Sun, 21 Dec 2008 00:00:48 +0100

libpam-ssh (1.92-3) experimental; urgency=low

  * Use all SSH keys in the special directory $HOME/.ssh/login-keys.d,
    thanks to Steve Langasek, Vincent Zweije, and Peter Palfrader.
  * Documented an alternative fall-through use, thanks to Luca Niccoli.
  * Avoid leaking information about the existence of users or presence of
    SSH login keys, thanks to Allan Wind.
    (Closes: #422453)
  * The cleanup of the way PAM exits are made also seems to have fixed the
    ssh-agent problems after failed login.
    (Closes: #425735)
  * Removed implementation-specific information from the manual page, and
    added more information suited for administrators.

 -- Jens Peter Secher <jps@debian.org>  Fri, 05 Dec 2008 17:27:40 +0100

libpam-ssh (1.92-2) experimental; urgency=low

  * Since format 3.0 (quilt) is not yet supported by dak (bug #457345),
    repack bzip source into a traditional gzip format.

 -- Jens Peter Secher <jps@debian.org>  Tue, 02 Dec 2008 17:06:42 +0100

libpam-ssh (1.92-1) experimental; urgency=low

  * New upstream release, which obsoletes some of the previous patches.
    (Closes: #437458)
  * Made the default PAM control be 'optional' instead of 'sufficient' so
    that other PAM modules on the stack will continue to execute, thanks
    to Christian Heimes.
    (Closes: #367586)
  * patches/enable-debug-info.diff: Make the debug option have an effect.
  * patches/fix-try-first-password.diff: Make pam_ssh behave like the
    manual page says it should, namely first trying the password from the
    preseding PAM module, and otherwise ask for a specific SSH password.
    In addition, it now completely refrains from asking passwords if there
    are no such SSH keys, and always use all SSH keys matching id_*
    (mostly thanks to a patch from Javier Serrano Polo), which means that
    the option keyfiles is now obsolete.
    (Closes: #350514,#336291,#477272)
  * Always use up-to-date config.{sub,guess} from autotools-dev.
  * Removed the pam- prefix from the pam modules so they are now simply
    called ssh-auth and ssh-session which is in line with other pam
    modules, hence the upload to experimental.
  * Updated dependencies from ssh to openssh, and removed ancient
    lower bounds on various version numbers.
    (Closes: #346393)
  * Clarified package description and documentation in general, and added
    explanation of option 'allow_blank_passphrase'.
  * Migrated to source package format 3.0 to use the upstream bzip2 tar
    ball directly, and migrated to quilt patch system while at it.
  * New maintainer.
    (Closes: #503487)

 -- Jens Peter Secher <jps@debian.org>  Sun, 30 Nov 2008 15:41:52 +0100

libpam-ssh (1.91.0-9.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fixed bashisms in postinst (closes: #472231)
  * Added Homepage control field
  * Added watch file
  * Call make distclean so that everything is cleaned, and don't ignore errors

 -- Peter Eisentraut <petere@debian.org>  Sun, 06 Apr 2008 10:20:51 +0200

libpam-ssh (1.91.0-9.2) unstable; urgency=low

  * Non-maintainer upload by testing security team.
  * Include 03_fix-CVE-2007-0844 to fix authentication bypass if
    allow_blank_passphrase is enabled (CVE-2007-0844) (Closes: #410236).
  * Included 04_fix_syslogh_inclusion.dpatch to fix missing inclusion
    of syslog headers which lead to FTBFS.

 -- Nico Golde <nion@debian.org>  Thu, 30 Aug 2007 16:55:51 +0200

libpam-ssh (1.91.0-9.1) unstable; urgency=low

  * Non-Maintainer Upload.
  * Include <openssl/md5.h> to fix FTBFS, patch from Stefan
    Fritsch. (Closes: #334916)

 -- Steinar H. Gunderson <sesse@debian.org>  Thu, 19 Jan 2006 00:00:47 +0100

libpam-ssh (1.91.0-9) unstable; urgency=high

  * Urgency set to high due to a RC bugfix
  * Fix dpatch debian/rules integration
  * Add patch from Dmitry K. Butskoj <buc@users.sourceforge.net> that
    closes: #301204 (usecure use of getpwnam())
  * add a postinst script to remove trailing /etc/init.d/libpam-ssh script
    installed with a old version and no longer used

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Fri, 22 Apr 2005 21:37:21 +0200

libpam-ssh (1.91.0-8) unstable; urgency=low

  * Introduction of dpatch
  * Add patch to cleanup old ~/.ssh/agent-[host]-[tty] files.
    (courtesy of Itay Ben-Yaacov <nib_maps@yahoo.com>)
    Closes: #294181
  * Revert prio of pam-ssh-session to optional. Revert #286645
    Closes: #296772

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Tue,  8 Mar 2005 09:34:44 +0100

libpam-ssh (1.91.0-7) unstable; urgency=low

  * not released
  * Wrap README.Debian to 80 chars/line. Closes: #293916

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Tue,  8 Feb 2005 14:41:43 +0100

libpam-ssh (1.91.0-6) unstable; urgency=low

  * Remove /lib/security/pam_ssh.la, as it's not needed after compilation/link.
    libtool --finish. Closes: #291505

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Fri, 21 Jan 2005 11:04:13 +0100

libpam-ssh (1.91.0-5) unstable; urgency=low

  * Pam-ssh-auth tweaks (Thanks to Johannes Gijsbers) Closes: #286645
  * Update dependencies from "ssh" to "ssh | ssh-krb5" Closes: #288093
  * Some corrections into debian/rules to make lintian totally quiet

 -- Christian Bayle <bayle@debian.org>  Mon, 10 Jan 2005 14:16:55 +0100

libpam-ssh (1.91.0-4) unstable; urgency=low

  * Provide working configure script to avoid problems while its generation
    (bootstrap.sh script to rebuild it also provided)
  * Closes ITP Closes: #156257

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Thu, 25 Nov 2004 10:01:25 +0100

libpam-ssh (1.91.0-3) unstable; urgency=low

  * Fix build dependencies
  * Fix compilation warning about some m4 files (clean target update)

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Wed,  3 Nov 2004 17:22:19 +0100

libpam-ssh (1.91.0-2) unstable; urgency=low

  * Fix man installation
  * Recommends ssh in place of require it (not needed for install, even if
    needed for use)

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Thu, 28 Oct 2004 10:46:45 +0200

libpam-ssh (1.91.0-1) unstable; urgency=low

  * Fix pam configuration problem, updated the Readme.Debian file
  * New upstream release

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Wed, 22 Sep 2004 12:19:07 +0200

libpam-ssh (1.9.0-2) unstable; urgency=low

  * Installation script bugfix (duplicated manpage)

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Mon, 23 Feb 2004 09:34:35 +0100

libpam-ssh (1.9.0-1) unstable; urgency=low

  * New upstream realease
  * Updated for newer Autoconf and Automake

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Fri, 20 Feb 2004 00:42:21 +0100

libpam-ssh (1.8.0-2) unstable; urgency=low

  * Licence type fix, README.Debian fix

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Mon, 16 Feb 2004 13:29:21 +0100

libpam-ssh (1.8.0-1) unstable; urgency=low

  * Initial Package release.

 -- Aurelien Labrosse <aurelien.labrosse@free.fr>  Sun, 15 Feb 2004 19:59:55 +0100
